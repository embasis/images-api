#!/bin/sh

restart_node () {
    [ -n "$SLEEP_PID" ] && kill -9 $SLEEP_PID
    [ -n "$NODE_PID" ] && kill -9 $NODE_PID
    export PORT=3001
    export DB_CONNECTION_STRING="$(secret-tool lookup source mongodbimagesapi)"
    export TOKEN_SIGNING_KEY="$(secret-tool lookup source imagesapitokensign)"
    export SESSION_SIGNING_KEY="$(secret-tool lookup source imagesapisessionsign)"
    export DOMAIN="http://192.168.100.9:$PORT"
    export APP_DOMAIN="http://192.168.100.9:3000"
    export SECURE_COOKIES=0
    export TEMPDIR=/tmp/images-tmp
    node build/main.js &
    NODE_PID=$!
}

exit_f () {
    [ -n "$SLEEP_PID" ] && kill -9 $SLEEP_PID
    [ -n "$NODE_PID" ] && kill -9 $NODE_PID
    exit
}

trap 'exit_f' EXIT
trap 'exit_f' INT
trap "restart_node" USR1

while true; do
    sleep infinity &
    SLEEP_PID=$!
    wait $!
done
