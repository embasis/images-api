#!/bin/sh

exit_f () {
    kill -9 $!
    exit 0
}

trap 'exit_f' INT

BUILD_PROCESS_PID_FILE=/tmp/build_process_pid
./build_process.sh &
printf "%d" $! > $BUILD_PROCESS_PID_FILE
wait
