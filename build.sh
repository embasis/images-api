#!/bin/sh

BUILD_PROCESS_PID_FILE=/tmp/build_process_pid
tsc
kill -USR1 $(cat /tmp/build_process_pid)