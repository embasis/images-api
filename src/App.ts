import * as Express from 'express';
import Api from './Api';
import * as CookieParser from 'cookie-parser';
import * as CORS from 'cors';

export default class App {
    private readonly appExpress: Express.Express;
    private readonly api: Api;

    constructor() {
        this.appExpress = Express.default();

        this.appExpress.use(Express.json());
        this.appExpress.use(CookieParser.default());
        this.appExpress.use(CORS.default({
            origin: process.env.APP_DOMAIN,
            optionsSuccessStatus: 200,
            credentials: true
        }));

        this.api = new Api;
        this.api.attachRouteHandlers(this.appExpress);
        this.appExpress.get('/*', (req, res) => {
            res.sendStatus(400);
        })
    }

    listen(): void {
        this.appExpress.listen(process.env.PORT || 443);
    }
}