import JWT from 'jsonwebtoken';

export enum TokenType {
    UserValidation,
    PasswordReset
}

export interface TokenPayload<PayloadType> {
    iat: number,
    exp: number,
    payloadData: PayloadType,
    tokenType: TokenType
}

const TOKEN_SIGNING_KEY = <string>process.env.TOKEN_SIGNING_KEY;
export function createToken(type: TokenType, expiresIn: string | number | undefined,
    payload: any): string {
    let tokenPayload = {
        tokenType: type,
        payloadData: payload
    }

    return JWT.sign(tokenPayload, TOKEN_SIGNING_KEY, {expiresIn: expiresIn});
}

export function verifyToken<PayloadType>(token: string, type: TokenType): TokenPayload<PayloadType> | null {
    try {
        let payload = <TokenPayload<PayloadType>>JWT.verify(token, TOKEN_SIGNING_KEY, {algorithms: ['HS256']});
        if (type !== payload.tokenType) {
            return null;
        }
        return payload;
    } catch (e) {
        return null;
    }
}