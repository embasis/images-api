import Container from 'typedi';
import Mongoose from 'mongoose';
import MailService from './MailService';
import * as Token from './Token';
import Argon2 from 'argon2';

export default class UserService {
    private readonly userSchema: Mongoose.Schema;
    private readonly userModel: Mongoose.Model<Mongoose.Document, {}>;
    private static readonly TOKEN_SIGNING_KEY = <string>process.env.TOKEN_SIGNING_KEY;
    private readonly mailService: MailService;

    constructor() {
        this.mailService = Container.get(MailService);
        this.userSchema = new Mongoose.Schema({
            email: {
                type: Mongoose.SchemaTypes.String,
                match: /.+@.+/,
                lowercase: true,
                unique: true,
                required: true
            },
            password: {
                type: Mongoose.SchemaTypes.String,
                required: true
            },
            passwordChangeTimestamp: Mongoose.SchemaTypes.Number
        });
        this.userModel = Mongoose.model('User', this.userSchema);
    }

    private async hashPassword(password: string): Promise<string | null> {
        if (password.length >= 6 && /[0-9]/.test(password) && /[a-z]/.test(password) && /[A-Z]/.test(password)) {
            return Argon2.hash(password);
        }
        return null;
    }

    private createToken(type: Token.TokenType, expiresIn: string, userID: string) {
        return Token.createToken(type, expiresIn, userID);
    }

    private async verifyToken(token: string, type: Token.TokenType): Promise<string | null> {
        let payload = Token.verifyToken<string>(token, type);
        if (!payload) {
            return null;
        }

        let user = await this.userModel.findById(payload.payloadData);
        if (!user || user.get('passwordChangeTimestamp') > payload.iat) {
            return null;
        }

        return payload.payloadData;
    }

    async signUp(email: string, password: string, validationApiURL: string): Promise<string> {
        let info = await this.userModel.findOne({email: email});
        if (info && info.get('passwordChangeTimestamp')) {
            return 'alreadyexists';
        }

        let hash = await this.hashPassword(password);
        if (!hash) {
            return 'badpassword';
        }

        try {
            let document = await this.userModel.findOneAndUpdate({email: email}, {$set: {
                email: email,
                password: hash,
            }}).setOptions({upsert: true, returnNewDocument: true});

            let token = this.createToken(Token.TokenType.UserValidation, '24h', (<Mongoose.Document>document).id);
            await this.mailService.sendNotificationEmail(email, `${validationApiURL}?token=${token}`);
            return 'success';
        } catch (e) {
            console.log(e);
            if (e.errors && e.errors.email) {
                return 'bademail';
            }
            return 'error';
        }
    }

    async login(email: string, password: string): Promise<string | null> {
        let info = await this.userModel.findOne({email: email});
        if (!info) {
            return null;
        }

        if (!await Argon2.verify(info.get('password').valueOf(), password)) {
            return null;
        }

        return info.id;
    }

    async sendPasswordResetLink(email: string, passwordResetURL: string): Promise<boolean> {
        let info = await this.userModel.findOne({email: email});
        if (!info) {
            return false;
        }

        let token = this.createToken(Token.TokenType.PasswordReset, '5m', info.id);
        await this.mailService.sendPasswordChangeEmail(email, `${passwordResetURL}?token="${token}"`);

        return true;
    }

    async validate(token: string): Promise<boolean> {
        let userID = await this.verifyToken(token, Token.TokenType.UserValidation);
        if (!userID) {
            return false;
        }

        let info = await this.userModel.findById(userID);
        if (!info) {
            console.log('validate: info is null!!!!');
            return false;
        }
        info.set('passwordChangeTimestamp', Math.floor(Date.now() / 1000));
        try {
            await info.save();
        } catch(e) {
            console.log(e);
            return false;
        }
        return true;
    }
}