import ChildProcess from 'child_process';
import FS from 'fs';

export interface ImageMetadata {
    width: number,
    height: number
}

export function getMetadata(filePath: string): Promise<ImageMetadata> {
    return new Promise((resolve, reject) => {
        ChildProcess.exec(`identify -format '%w %h' ${filePath}`, (error, stdout, stderr) => {
            if (error) {
                reject(error.message);
            }
            if (stderr.length || !stdout.length) {
                reject('Bad image');
            }

            let result = stdout.split(' ');
            if (result.length !== 2) {
                reject('Bad image');
            }

            resolve({width: parseInt(result[0]), height: parseInt(result[1])});
        });
    })
}

export function resizeImage(filePath: string, nWidth: number, nHeight: number): Promise<string> {
    return new Promise((resolve, reject) => {
        let resultPath = `${filePath}-${nWidth}-${nHeight}`;
        ChildProcess.exec(`convert ${filePath} -resize ${nWidth}x${nHeight}\\! ${resultPath}`,
        (error, stdout, stderr) => {
            if (error) {
                reject(error.message);
            }
            if (stderr.length) {
                reject('Bad resize command');
            }
            FS.promises.access(resultPath).then(() => resolve(resultPath)).catch(() => reject('Bad resize command'));
        });
    })
}