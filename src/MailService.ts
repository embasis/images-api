import Nodemailer from 'nodemailer';
import Mail from 'nodemailer/lib/mailer';

export namespace Transport {
    export async function EtherealMail() {
        let testAccount = await Nodemailer.createTestAccount();

        console.log('EtherealMail test account');
        console.log('user: ', testAccount.user, '; pass: ', testAccount.pass);

        return Nodemailer.createTransport({
            host: testAccount.smtp.host,
            port: testAccount.smtp.port,
            secure: testAccount.smtp.secure,
            auth: {
                user: testAccount.user,
                pass: testAccount.pass
            }
        })
    }
}

export default class MailService {

    constructor(private transport: Mail, private fromAddress: string) {
    }

    sendNotificationEmail(to: string, content: string): Promise<Nodemailer.SentMessageInfo> {
        return this.transport.sendMail({
            from: this.fromAddress,
            to: to,
            subject: 'Images Email Verification',
            text: `Your email address was used to signup to the Images service. Use the following link to complete the signup process:
            ${content}`
        });
    }

    sendPasswordChangeEmail(to: string, content: string): Promise<Nodemailer.SentMessageInfo> {
        return this.transport.sendMail({
            from: this.fromAddress,
            to: to,
            subject: 'Password Change Request',
            text: `Your email address was used to request a password change on the Images service. Use the following link to complete the
            password change: ${content}`
        })
    }
}