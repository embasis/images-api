import 'reflect-metadata';
import App from './App';
import Mongoose from 'mongoose';
import {Container} from 'typedi'; 
import MailService, {Transport} from './MailService';
import UserService from './UserService';
import ImageServer from './ImageServer';
import ImageService from './ImageService';

Mongoose.connect(<string>process.env.DB_CONNECTION_STRING, {dbName: 'Main', useNewUrlParser: true}).catch((r: any) => {
    console.log('connect rejected: ', r.toString());
});
Mongoose.connection.on('error', (e: any) => {
    console.log('MongoDB error: ', e.toString());
});
Mongoose.connection.once('open', () => {
    Mongoose.mongo.connect(<string>process.env.DB_CONNECTION_STRING, {useNewUrlParser: true})
    .then((client) => {
        Container.set(ImageServer, new ImageServer(client));
        Container.set(ImageService, new ImageService());
        Transport.EtherealMail().then((transport) => {
            Container.set(MailService, new MailService(transport, 'no-reply@images.fakedomain'));
            Container.set(UserService, new UserService());
            Container.get(App).listen();
        });
    })
    .catch((e) => {
        console.log('connect failed');
        console.log(e);
    });
});