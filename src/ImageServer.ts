import {MongoClient, GridFSBucket} from 'mongodb';
import Mongoose from 'mongoose';
import {Readable} from 'stream';

export default class ImageServer {
    private readonly bucket: GridFSBucket;
    private readonly idConversionSchema: Mongoose.Schema;
    private readonly idConversionModel: Mongoose.Model<Mongoose.Document, {}>;
    private static readonly DB_NAME = 'ImageFiles';

    constructor(private mongoClient: MongoClient) {
        if (!mongoClient.isConnected()) {
            throw new Error('MongoDB client must be connected before initializing ImageServerGridFS!');
        }
        this.bucket = new GridFSBucket(mongoClient.db(ImageServer.DB_NAME));
        
        this.idConversionSchema = new Mongoose.Schema({
            imageID: Mongoose.SchemaTypes.ObjectId,
            mipLevel: Mongoose.SchemaTypes.Number,
            imageFileID: Mongoose.SchemaTypes.ObjectId
        });
        this.idConversionSchema.index({imageID: 1, mipLevel: 1});
        this.idConversionModel = Mongoose.model('ImageIDConversion', this.idConversionSchema);
    }

    private static makeFileName(imageID: string, mipLevel: number) {
        return `${imageID}-${mipLevel}`;
    }

    uploadImage(imageID: string, mipLevel: number, data: Readable): Promise<void> {
        return new Promise((resolve, reject) => {
                let stream = data.pipe(this.bucket.openUploadStream(ImageServer.makeFileName(imageID, mipLevel)));
                stream.on('error', (e) => {
                    reject('Bad image');
                })
                .on('finish', () => {
                    this.idConversionModel.create({
                        imageID: imageID,
                        mipLevel: mipLevel,
                        imageFileID: stream.id
                    }).then((_) => resolve());
                });
            }
        );
    }

    async getImage(imageID: string, mipLevel: number): Promise<Readable | null> {
        let document = await this.idConversionModel.findOne({imageID: imageID, mipLevel: mipLevel});
        if (document === null) {
            return null;
        }
        return this.bucket.openDownloadStream(document.get('imageFileID'));
    }

    async removeImage(imageID: string, mipLevel: number): Promise<boolean> {
        let document = await this.idConversionModel.findOne({imageID: imageID, mipLevel: mipLevel});
        if (document === null) {
            return false;
        }

        this.bucket.delete(document.get('imageFileID'));

        return true;
    }

    async removeAllMips(imageID: string): Promise<boolean> {
        let docs;
        try {
            docs = await this.idConversionModel.find({imageID: imageID});
            if (!docs.length) {
                return false;
            }
        } catch(e) {
            return false;
        }
        docs.map((doc) => doc.get('imageFileID')).forEach((id) => this.bucket.delete(id));
        await this.idConversionModel.deleteMany({imageID: imageID});
        return true;
    }
}