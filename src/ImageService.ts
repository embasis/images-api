import Mongoose from 'mongoose';
import ImageServer from './ImageServer';
import * as ImageMagick from './ImageMagick';
import {Container} from 'typedi';
import {ObjectId} from 'mongodb';
import FS from 'fs';
import { Readable } from 'stream';

export enum SortCriteria {
    Name,
    CreationDate,
    ChangeDate
}

export enum ResourceType {
    Gallery,
    Image
}

export enum AuthorizationCheckResult {
    Authorized,
    Unauthorized,
    ResourceNotFound
}

export interface SearchProps {
    resourceType: ResourceType,
    createTime: number,
    galleryID?: string,
    globalSearch?: boolean,
    order?: SortCriteria, 
    descending?: boolean,
    startDate?: Date,
    endDate?: Date,
    searchPrefix?: string,
    dateSearchField?: string,
}

export interface ResourceBase {
    name: string,
    createdAt: Date,
    lastChanged: Date,
}

export interface ResourceImage {
    galleryID: string,
    mimeType: string,
    width: number,
    height: number,
}

export interface ResourceGallery {
    imageCount: number
}

export interface ImageData {
    name: string,
    mipLevel: number,
    mimeType: string,
    data: Readable
}

export type Image = ResourceBase & ResourceImage
export type Gallery = ResourceBase & ResourceGallery
export type Query = Mongoose.DocumentQuery<Mongoose.Document[], Mongoose.Document, {}>

interface MipLevel {
    image: string,
    size: number
}

export default class ImageService {
    private static readonly mipWidthSizes = [512, 1024, 2048];
    private static readonly mipHeightSizes = [512, 1024, 2048];
    private readonly gallerySchema: Mongoose.Schema;
    private readonly galleryModel: Mongoose.Model<Mongoose.Document, {}>;
    private readonly imagesSchema: Mongoose.Schema;
    private readonly imagesModel: Mongoose.Model<Mongoose.Document, {}>;
    private readonly imageServer: ImageServer;

    constructor() {
        this.imageServer = Container.get(ImageServer);

        this.gallerySchema = new Mongoose.Schema({
            userID: Mongoose.SchemaTypes.ObjectId,
            name: Mongoose.SchemaTypes.String,
            nameLowercase: Mongoose.SchemaTypes.String,
            createdAt: Mongoose.SchemaTypes.Date,
            lastChanged: Mongoose.SchemaTypes.Date,
            imageCount: Mongoose.SchemaTypes.Number
        });

        this.gallerySchema.index({userID: 1, nameLowercase: 1}, {unique: true});
        this.gallerySchema.index({userID: 1, lastChanged: 1});
        this.gallerySchema.index({userID: 1, createdAt: 1});

        this.imagesSchema = new Mongoose.Schema({
            userID: Mongoose.SchemaTypes.ObjectId,
            galleryID: Mongoose.SchemaTypes.ObjectId,
            name: Mongoose.SchemaTypes.String,
            nameLowercase: Mongoose.SchemaTypes.String,
            mimeType: Mongoose.SchemaTypes.String,
            width: Mongoose.SchemaTypes.Number,
            height: Mongoose.SchemaTypes.Number,
            createdAt: Mongoose.SchemaTypes.Date,
            lastChanged: Mongoose.SchemaTypes.Date,
            mipLevels: [Mongoose.SchemaTypes.Number] // smallest to biggest
        });

        this.imagesSchema.index({userID: 1, nameLowercase: 1}, {unique: true});
        this.imagesSchema.index({userID: 1, lastChanged: 1});
        this.imagesSchema.index({userID: 1, createdAt: 1});
        this.imagesSchema.index({galleryID: 1, nameLowercase: 1}, {unique: true});
        this.imagesSchema.index({galleryID: 1, lastChanged: 1});
        this.imagesSchema.index({galleryID: 1, createdAt: 1});

        this.galleryModel = Mongoose.model('Gallery', this.gallerySchema);
        this.imagesModel = Mongoose.model('Images', this.imagesSchema);
    }

    async isUserAuthorized(userID: string, resourceID: string, resourceType: ResourceType): Promise<AuthorizationCheckResult> {
        let image;
        if (resourceType === ResourceType.Image) {
            image = await this.imagesModel.findById(resourceID);
            if (image === null) {
                return AuthorizationCheckResult.ResourceNotFound;
            }
        }

        let gallery = await this.galleryModel.findById(
            resourceType === ResourceType.Image ? (<Mongoose.Document>image).get('galleryID') : resourceID);
        if (gallery === null) {
            return AuthorizationCheckResult.ResourceNotFound;
        }
        if (gallery.get('userID').toHexString() === userID) {
            return AuthorizationCheckResult.Authorized;
        }

        return AuthorizationCheckResult.Unauthorized;
    }

    private static orderToSortCriteria(order: SortCriteria, descendingOrder: Boolean): object {
        let mongoSortOrder = descendingOrder ? -1 : 1;
        switch (order) {
            case SortCriteria.Name:
                return {name: mongoSortOrder};
            case SortCriteria.ChangeDate:
                return {lastChanged: mongoSortOrder};
            case SortCriteria.CreationDate:
                return {createdAt: mongoSortOrder};
            default:
                throw new Error(`Unrecognized SortCriteria: ${SortCriteria[order]}`)
        }
    }

    private getResourceListQuery(sourceModel: Mongoose.Model<Mongoose.Document, {}>, queryID: string, globalSearch: boolean,
                         order: SortCriteria, descendingOrder: boolean): Query {
        let conditions = globalSearch ? {userID: queryID} : {galleryID: queryID};
        return sourceModel.find(conditions).sort(ImageService.orderToSortCriteria(order, descendingOrder));
    }

    private searchNameQuery(prefix: string | undefined, query: Query): Query {
        if (!prefix) {
            return query;
        }

        let escapedPrefix = prefix.toLowerCase().replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
        query.regex('nameLowercase', new RegExp(`^${escapedPrefix}`));

        return query;
    }

    async getResourceList(resourceType: ResourceType, queryID: string, searchPrefix?: string, startCreatedAtDate?: Date,
                          endCreatedAtDate?: Date, startLastChangedDate?: Date, endLastChangedDate?: Date, globalSearch = true,
                          order = SortCriteria.Name, descendingOrder = false): Promise<string[]> {

        let query = this.getResourceListQuery(resourceType === ResourceType.Image ? this.imagesModel : this.galleryModel,
            queryID, globalSearch, order, descendingOrder);
        
        this.searchNameQuery(searchPrefix, query);

        if (endCreatedAtDate) {
            query.where('createdAt').lt(endCreatedAtDate);
        }
        if (startCreatedAtDate) {
            query.where('createdAt').gt(startCreatedAtDate);
        }
        if (endLastChangedDate) {
            query.where('lastChanged').lt(endLastChangedDate);
        }
        if (startLastChangedDate) {
            query.where('lastChanged').gt(startLastChangedDate);
        }

        return (await query).map((doc) => doc.id);
    }

    async getResource(resourceType: ResourceType, id: string): Promise<Image | Gallery | null> {
        const convertImage = (doc: Mongoose.Document) => {
            return <Image>{
                name: doc.get('name').valueOf(),
                createdAt: doc.get('createdAt'),
                lastChanged: doc.get('lastChanged'),
                galleryID: doc.get('galleryID').valueOf(),
                mimeType: doc.get('mimeType').valueOf(),
                width: doc.get('width').valueOf(),
                height: doc.get('height').valueOf()
            }
        }
        const convertGallery = (doc: Mongoose.Document) => {
            return <Gallery>{
                name: doc.get('name').valueOf(),
                createdAt: doc.get('createdAt'),
                lastChanged: doc.get('lastChanged'),
                imageCount: doc.get('imageCount').valueOf()
            }
        }
        let doc = await (resourceType === ResourceType.Image ? this.imagesModel : this.galleryModel).findById(id);
        if (!doc) {
            return null;
        }
        return resourceType == ResourceType.Image ? convertImage(doc) : convertGallery(doc);
    }

    async updateGallery(galleryID: string, imageCountDelta: number, time: Date): Promise<boolean> {
        let gallery = await this.galleryModel.findById(galleryID)
        if (!gallery) {
            console.log('updateGallery: gallery is null!!!!');
            return false;
        }
        try {
            gallery.set('imageCount', gallery.get('imageCount').valueOf() + imageCountDelta);
            gallery.set('lastChanged', time);
            await gallery.save();
        } catch (e) {
            console.log(e);
            return false;
        }
        return true;
    }

    async createGallery(userID: string, name: string): Promise<boolean> {
        let time = new Date();

        try {
            await this.galleryModel.create({
                userID: userID,
                name: name,
                nameLowercase: name.toLowerCase(),
                createdAt: time,
                lastChanged: time,
                imageCount: 0
            })
        } catch (e) {
            console.log(e);
            return false;
        }

        return true;
    }

    async removeGallery(galleryID: string): Promise<boolean> {
        try {
            let images = await this.imagesModel.find({galleryID: galleryID});
            for (const img of images) {
                if (!await this.removeImage(img.get('id'))) {
                    return false;
                }
            }
        } catch {
            return false;
        }

        try {
            await this.galleryModel.deleteOne({_id: galleryID});
        } catch {
            return false;
        }

        return true;
    }

    private async generateMipLevels(filePath: string, metadata: ImageMagick.ImageMetadata): Promise<MipLevel[]> {
        let mips: MipLevel[] = [], isHeight: boolean, ratio: number, mainSize, mipSizes: number[];

        if (metadata.width < metadata.height) {
            isHeight = true;
            ratio = metadata.width / metadata.height;
            mipSizes = ImageService.mipHeightSizes;
            mainSize = metadata.height;
        } else {
            isHeight = false;
            ratio = metadata.height / metadata.width;
            mipSizes = ImageService.mipWidthSizes;
            mainSize = metadata.width;
        }

        for (let i = 0; i < mipSizes.length && mipSizes[i] < mainSize; ++i) {
            let size = mipSizes[i];
            mips.push({image: await ImageMagick.resizeImage(filePath,
                isHeight ? Math.floor(size * ratio) : size, isHeight ? size : Math.floor(size * ratio)), size: size});
        }

        mips.push({image: filePath, size: mainSize});

        return mips;
    }

    async uploadImage(galleryID: string, name: string, mimeType: string, lastChanged: Date, filePath: string): Promise<string> {
        try {
            if (!['image/jpeg', 'image/pjpeg', 'image/jpm', 'image/jpx', 'image/png'].includes(mimeType)) {
                return 'badmime';
            }

            let metadata = await ImageMagick.getMetadata(filePath),
                mips = (await this.generateMipLevels(filePath, metadata)),
                imageID = new ObjectId(),
                time = new Date();

            let imageDoc = await this.imagesModel.create({
                _id: imageID,
                galleryID: galleryID,
                name: name,
                nameLowercase: name.toLowerCase(),
                mimeType: mimeType,
                width: metadata.width,
                height: metadata.height,
                mipLevels: mips.map(mip => mip.size),
                lastChanged: lastChanged,
                createdAt: time
            });

            let streams = mips.map((mip) => FS.createReadStream(mip.image)),
                cleanup = () => {
                    streams.forEach((s) => s.close());
                    return Promise.all(mips.map((mip) => new Promise((resolve, reject) => {
                        FS.unlink(mip.image, (err) => {
                            if (err) {
                                console.log(err);
                            }
                            resolve();
                        });
                    })));
                }

            try {
                for (let i = 0; i < mips.length; ++i) {
                    await this.imageServer.uploadImage(imageID.toHexString(), i, streams[i]);
                }
                this.updateGallery(galleryID, 1, time);
            } catch (e) {
                this.imageServer.removeAllMips(imageID.toHexString());
                this.imagesModel.findByIdAndDelete(imageDoc.id);
                cleanup();
                return 'error';
            }
            cleanup();
        } catch (e) {
            return 'error';
        }

        return 'success';
    }

    private calculateMipLevel(imageDoc: Mongoose.Document, clientWidth: number, clientHeight: number): number {
        let mipLevels = <Array<number>>imageDoc.get('mipLevels').map((x: Number) => x.valueOf());
        if (!clientWidth || !clientHeight) {
            return mipLevels.length - 1;
        }

        let mipLevel = 0, clientSize = imageDoc.get('width').valueOf() < imageDoc.get('height').valueOf() ? clientHeight : clientWidth;
        while (mipLevel < mipLevels.length && mipLevels[mipLevel] < clientSize) {
            ++mipLevel;
        }
        return mipLevel === mipLevels.length ? mipLevel - 1 : mipLevel;
    }

    async getImage(imageID: string, clientWidth: number, clientHeight: number): Promise<ImageData | null> {
        let imageDoc = await this.imagesModel.findById(imageID);
        if (!imageDoc) {
            return null;
        }

        let mipLevel = this.calculateMipLevel(imageDoc, clientWidth, clientHeight);
        console.log(mipLevel);
        let data = await this.imageServer.getImage(imageID, mipLevel);

        if (!data) {
            console.log('getImage: data is null!!!!!');
            return null;
        }

        return {name: imageDoc.get('name').valueOf(), mipLevel: mipLevel, mimeType: imageDoc.get('mimeType').valueOf(), data: data};
    }

    async removeImage(imageID: string): Promise<boolean> {
        let imgDoc, time = new Date();
        try {
            imgDoc = await this.imagesModel.findByIdAndDelete(imageID);
            if (!imgDoc) {
                return false;
            }
        } catch (e) {
            return false;
        }
        return await this.updateGallery(imgDoc.get('galleryID'), -1, time) && await this.imageServer.removeAllMips(imageID);
    }
}