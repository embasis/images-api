import Formidable from 'formidable';
import {Request, Response, RequestHandler, NextFunction} from 'express';

export function makeCookie(name: string, value: string) {
    return `${name}=${value}; Path=/; HttpOnly${process.env.SECURE_COOKIES ? '; Secure' : ''}`
}

export {Files as FilesFormData, File as FileFormData} from 'formidable';

export function parseForm(maxFileSize: number) {
    return (req: Request, res: Response, next: NextFunction) => {
        let form = new Formidable.IncomingForm();
        form.multiples = true;
        form.maxFileSize = maxFileSize;

        form.parse(req, (err, fields, files) => {
            if (err) {
                console.log('form parse err: ', err);
                req.body.files = {};
            } else {
                console.log(req.body);
                if (fields) {
                    Object.assign(req.body, fields);
                }
                console.log(req.body);
                req.body.files = files;
            }
            next();
        })
    }
}