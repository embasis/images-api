import {Express, Request, Response, NextFunction} from 'express';
import * as ExpressSession from 'express-session';
import connect from 'connect-mongodb-session';
import {Container} from 'typedi';
import UserService from './UserService';
import ImageService, {ResourceType, AuthorizationCheckResult} from './ImageService';
import * as Utils from './Utils';

export default class Api {
    private static readonly FORM_MAX_FILE_SIZE = 100 * 1024 * 1024;
    private static readonly SESSION_COLLECTION_NAME = 'Session';
    private static readonly MongoDBSessionStore = connect(ExpressSession.default);
    private readonly userService: UserService; 
    private readonly imageService: ImageService;
    static readonly versionString = 'v1';

    constructor() {
        this.userService = Container.get(UserService);
        this.imageService = Container.get(ImageService);
    }

    attachRouteHandlers(app: Express): void {
        const checkAuthGallery = this.checkAuthorization(ResourceType.Gallery, false).bind(this),
              checkAuthImage = this.checkAuthorization(ResourceType.Image, false).bind(this),
              checkAuthGalleryAllowQuery = this.checkAuthorization(ResourceType.Gallery, true).bind(this),
              checkAuthImageAllowQuery = this.checkAuthorization(ResourceType.Image, true).bind(this),
              failIfValidLogin = this.failIfValidLogin.bind(this),
              parseForm = Utils.parseForm(Api.FORM_MAX_FILE_SIZE);

        const postRouteHandlers = [['signup',          [failIfValidLogin, this.signUp.bind(this)], 0],
                                   ['login',           [failIfValidLogin, this.login.bind(this)], 0],
                                   ['logout',          [this.logout.bind(this)], 0],
                                   ['restorepassword', [failIfValidLogin, this.restorePassword.bind(this)], 0],
                                   ['checkauth',       [this.checkAuth.bind(this)], 0],
                                   ['images',          [this.resourceList.bind(this, ResourceType.Image)]],
                                   ['galleries',       [this.resourceList.bind(this, ResourceType.Gallery)]],
                                   ['deleteimage',     [checkAuthImage, this.deleteImage.bind(this)]],
                                   ['deletegallery',   [checkAuthGallery, this.deleteGallery.bind(this)]]];
        const putRouteHandlers =  [['uploadimage',     [parseForm, checkAuthGallery, this.uploadImage.bind(this)]],
                                   ['creategallery',   [this.createGallery.bind(this)]]];
        const getRouteHandlers =  [['validate',        [this.validate.bind(this)], 0],
                                   ['file',            [checkAuthImageAllowQuery, this.file.bind(this)]],
                                   ['image',           [checkAuthImageAllowQuery, this.resource.bind(this, ResourceType.Image)]],
                                   ['gallery',         [checkAuthGalleryAllowQuery, this.resource.bind(this, ResourceType.Gallery)]]];

        const attachRouteHandler = (attachFunction: any, h: any) => {
            let [route, handlers, needsAuth] = h,
                routeFull = `/${Api.versionString}${needsAuth !== 0 ? '/auth' : ''}/${route}`;
            console.log(routeFull);
            handlers.forEach((handler: Function) => attachFunction(routeFull, handler));
        };

        this.initSessions(app);
        postRouteHandlers.forEach(attachRouteHandler.bind(null, app.post.bind(app)));
        putRouteHandlers.forEach(attachRouteHandler.bind(null, app.put.bind(app)));
        getRouteHandlers.forEach(attachRouteHandler.bind(null, app.get.bind(app)));
    }

    private initSessions(app: Express) {
        let store = new Api.MongoDBSessionStore({
            uri: <string>process.env.DB_CONNECTION_STRING,
            collection: Api.SESSION_COLLECTION_NAME
        }, (e) => console.log(`MongoSessionStore errors: ${e}`));

        let sessionOptions: ExpressSession.SessionOptions = {
            cookie: {httpOnly: true, secure: process.env.SECURE_COOKIES && parseInt(process.env.SECURE_COOKIES) > 0 ? true : false},
            name: 'SID',
            resave: true,
            saveUninitialized: true,
            secret: <string>process.env.SESSION_SIGNING_KEY,
            store: store
        };

        app.use(ExpressSession.default(sessionOptions));
        app.use(`/${Api.versionString}/auth`, (req, res, next) => {
            if (req.session && req.session.userID) {
                req.body.userID = req.session.userID;
                next();
            } else {
                res.sendStatus(401);
            }
        });
    }

    private checkAuth(req: Request, res: Response) {
        if(req.session && req.session.userID) {
            res.sendStatus(200);
        } else {
            res.sendStatus(401);
        }
    }

    private async signUp(req: Request, res: Response) {
        console.log('SignUp request recieved');
        res.json({
            result: await this.userService.signUp(req.body.email,
                req.body.password, `${process.env.DOMAIN}/${Api.versionString}/validate`)
        });
    }

    private async validate(req: Request, res: Response) {
        console.log('Validate request recieved');
        res.send(await this.userService.validate(req.query.token) ? 'Email validation is successful! You can now log in!'
                                                                 : 'Invalid validation parameters');
    }

    private async login(req: Request, res: Response) {
        console.log('Login request recieved');

        if (!req.session) {
            res.json({result: 'error'});
        } else {
            let userID = await this.userService.login(req.body.email, req.body.password)
            if (userID) {
                req.session.userID = userID;
                res.json({result: 'success'});
            } else {
                res.json({result: 'badlogin'});
            }
        }
    }

    private async logout(req: Request, res: Response) {
        console.log('Logout request recieved');

        let result = await new Promise(resolve => {
            if (req.session) {
                req.session.destroy(e => e ? resolve('error') : resolve('success'));
            } else {
                resolve('success');
            }
        });

        res.json({result: result});
    }

    private async restorePassword(req: Request, res: Response) {
        console.log('Password change request recieved');
        if (!req.body.email) {
            res.sendStatus(400);
        }
        res.json({result: await this.userService.sendPasswordResetLink(req.body.email, 'stub')})
    }

    private async failIfValidLogin(req: Request, res: Response, next: NextFunction) {
        if (req.session && req.session.userID) {
            res.sendStatus(400);
        } else {
            next();
        }
    }

    private checkAuthorization(resourceType: ResourceType, allowQueryParameters: boolean) {
        return async function(req: Request, res: Response, next: NextFunction) {
            if (allowQueryParameters) {
                if (!req.body.resourceID) {
                    req.body.resourceID = req.query.resourceID;
                }
            }

            let authorization = await this.imageService.isUserAuthorized(req.body.userID, req.body.resourceID, resourceType);

            console.log(`Authorization check: resourceType=${ResourceType[resourceType]}, `,
                    `resourceID=${req.body.resourceID} Result=${AuthorizationCheckResult[authorization]}`);
            
            switch (authorization) {
                case AuthorizationCheckResult.Authorized:
                    next();
                    break;
                case AuthorizationCheckResult.Unauthorized:
                    res.sendStatus(403);
                    break;
                case AuthorizationCheckResult.ResourceNotFound:
                    res.sendStatus(404);
                    break;
                default:
                    throw new Error('Unhandled AuthorizationCheckResult case!');
            }
        }
    }

    private async resourceList(resourceType: ResourceType, req: Request, res: Response) {
        console.log('Resource list request recieved');
        let queryID = req.body.userID, globalSearch = true;
        if (resourceType === ResourceType.Image && req.body.galleryID) {
            let auth = await this.imageService.isUserAuthorized(req.body.userID, req.body.galleryID, ResourceType.Gallery);
            if (auth !== AuthorizationCheckResult.Authorized) {
                res.json({result: 'badgalleryid'});
                return;
            }
            queryID = req.body.galleryID;
            globalSearch = false;
        }
        let result = await this.imageService.getResourceList(resourceType, queryID,
            req.body.searchPrefix, req.body.startCreatedAtDate, req.body.endCreatedAtDate, req.body.startLastChangedDate,
            req.body.endLastChangedDate, globalSearch, req.body.order, req.body.descending);
        res.json({result: 'success', resources: result});
    }

    private async resource(resourceType: ResourceType, req: Request, res: Response) {
        console.log('Resource request recieved');
        let result = await this.imageService.getResource(resourceType, req.body.resourceID);
        if (result) {
            res.json({result: 'success', resource: result});
        } else {
            res.json({result: 'error'})
        }
    }

    private async uploadImage(req: Request, res: Response) {
        console.log('Image upload request recieved');
        if (!req.body.files) {
            res.sendStatus(400);
        } else {
            let file = (<Utils.FilesFormData>req.body.files).image;
            if (!file || Array.isArray(file)) {
                res.sendStatus(400);
            } else {
                res.json({result: await this.imageService.uploadImage(req.body.resourceID, file.name, file.type,
                    file.lastModifiedDate ? file.lastModifiedDate : new Date(Date.now()), file.path)});
            }
        }
    }

    private async deleteImage(req: Request, res: Response) {
        console.log('Delete image request recieved');
        res.json({result: this.imageService.removeImage(req.body.resourceID) ? 'success' : 'error'});
    }

    private async createGallery(req: Request, res: Response) {
        console.log('Create gallery request recieved');
        if (!req.body.name) {
            res.sendStatus(400);
        } else {
            res.json({result: await this.imageService.createGallery(req.body.userID, req.body.name) ? 'success' : 'error'});
        }
    }

    private async deleteGallery(req: Request, res: Response) {
        console.log('Delete gallery request recieved');
        res.json({result: await this.imageService.removeGallery(req.body.resourceID) ? 'success' : 'error'});
    }

    private async file(req: Request, res: Response) {
        console.log('File request recieved');
        if (!req.body.w) {
            req.body.w = req.query.w;
        }
        if (!req.body.h) {
            req.body.h = req.query.h;
        }

        let imgData = await this.imageService.getImage(req.body.resourceID, req.body.w, req.body.h);

        if (!imgData) {
            res.sendStatus(404);
        } else {
            res.status(200);
            res.setHeader('Content-Type', imgData.mimeType);
            res.setHeader('Content-Disposition', `${req.body.download ? 'attachment' : 'inline'}; filename="${imgData.name}"`);
            imgData.data.pipe(res);
        }
    }
}